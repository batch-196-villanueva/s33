// express package was imported as express
const express = require("express");
// invoked express package to create a server/api and saved it in variable which we can refer to later to create routes.

const app = express();
// variable for port assignment


// app.use() is a method used to run another
app.use(express.json())

const port = 4000;
// used the listen() method of express to assign a port to our server and send a message

//mock collection of courses:
let courses = [

	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn React",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}

];

let users = [
    
    {
        email: "marybell_knight",
        password: "merrymarybell"
    },
    {
        email: "janedoePriest",
        password: "jobPriest100"
    },
    {
        email: "kimTofu",
        password: "dubuTofu"
    }
];

// creating a route in Express:
// access express to have access to its route methods.

/*
    app.method('/endpoint',(request,response)=>{

        //send() is a method similar to end() that it sends the data/message and ends the response
        // It also automatically creates and adds the headers.

        response.send()

    })
*/

app.get('/',(req,res) => {

    res.send("Hello from our first ExpressJS route!");

})

app.post('/',(req,res) => {

    res.send("Hello from our first ExpressJS Post Method route!");

})

app.put('/',(req,res) => {

    res.send("Hello from our first ExpressJS PUT Method route!");

})

app.delete('/',(req,res) => {

    res.send("Hello from our first ExpressJS DELETE Method route!");

})

app.get('/courses',(req,res)=>{

    res.send(courses)
})

// Create a route to be able to add a new course from an input from a request.

app.post('/courses',(req,res)=>{
    // with express.json() the data stream has been captured, the data input has been parsed into a JS Obect.

    // Note: Every time you need to access or use the request body, log it in the console first.
    
    // console.log(req.body); //object
    // request/req.body contains the body of the request or the input passed.

    // Add the input as req.body into our courses array
    courses.push(req.body);

    // console.log(courses);

    // Send the updated courses array in the client
    res.send(courses);
})

// Activity

// Create a new route to get and send the users array in the client.
app.get('/users',(req,res)=>{
    res.send(users);
})

// Create a new route to create and add a new user object in the users array.
app.post('/users',(req,res)=>{
    users.push(req.body)
    res.send(users)
})

// Stretch goal 1

app.delete('/users',(req,res)=>{
    users.pop();
    res.send("Last user has been deleted. Here's the new list of users:\n" + JSON.stringify(users))
})

// Stretch goal 2

app.put('/courses',(req,res)=>{
    courses[req.body.index].price = req.body.price
    res.send(courses[req.body.index]);
})


app.listen(port, () => console.log(`Express API running at port 4000`))